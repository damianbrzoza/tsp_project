import numpy as np
from typing import List
from town_generator import City


def plan_route(distances: np.ndarray) -> List[City]:
    raise NotImplementedError