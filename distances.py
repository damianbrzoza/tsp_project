from typing import List
from town_generator import City
import numpy as np


def get_distances(lst_of_cities: List[City]) -> np.ndarray:
    raise NotImplementedError
